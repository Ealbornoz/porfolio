// Java no es lo mismo que JavaScript

// ------------------------------------------ 03_05_2022 -----------------------------------------------------

 

/*

    3 formas de incorporar js a html

 

    JS en linea: Como atributo de los tags que empleamos. No es recomendado

    Utilizando <script>

    Archivos externos: Como crecen exponencialmente, es mas efectivo separarlo del archivo principal, ahorrar tiempo, etc.

 

    3 TIPOS DE DECLARACION DE VARIABLE

 

    var

    const

    let     Se utiliza mas que nada para variables que están dentro de la función.

 

*/

 

console.log("OAJSNAJSDN"); //entre comillas en los paréntesis, va el mensaje que aparece en la consola. Es como el Serial.println("...");

 

var nombre = "Emanuel";

var apellido = "Albornoz";

var dni = 45892342; //Tambien pueden ser valores con coma, racionales.

 

console.log(nombre);

console.log(apellido);

 

console.log("Mi nombre es: " + nombre + " " + apellido);

console.log("Mi DNI es: " + dni);

 

//ejemplo. Apuntar a un elemento con un id especifico.

 var correoRef = document.getElementById("") // el valor de retorno es una referencia al primer objeto con ese ID específico.

console.log(correoRef);

 

var botonRef = document.getElementById("passwordId");

console.log(botonRef);